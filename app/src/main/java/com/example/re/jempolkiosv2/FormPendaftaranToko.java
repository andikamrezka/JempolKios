package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class FormPendaftaranToko extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pendaftaran_toko);
        Button buat = (Button) findViewById(R.id.buat);

        buat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FormPendaftaranToko.this, TokoBerhasil.class);
                startActivity(intent);
            }
        });
    }
}
