package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RegistrasiBerhasil extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi_berhasil);

        TextView tv_rb = (TextView) findViewById(R.id.tv_br);
        tv_rb.setText(" Hallo, Mitra Jempol \n \n " +
                "Selamat Akun Jempol Anda berhasil didaftarkan. Kini \n " +
                "Anda dapat mengelola toko fisik, Toko Online dan \n " +
                "Marketplace Anda langsung dari genggaman tangan.\n \n " +
                "Anda dapat langsung masuk ke Dashboard atau " +
                "Daftarkan Toko Anda sendiri.\n " +
                "\n");

        LinearLayout daftar_toko = (LinearLayout) findViewById(R.id.daftar_toko);

        LinearLayout dashboard = (LinearLayout) findViewById(R.id.dashboard);

        dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrasiBerhasil.this, ProfilSebelumPunyaToko.class);
                startActivity(intent);
            }
        });

        daftar_toko.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistrasiBerhasil.this, FormPendaftaranToko.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromright, R.anim.slidetoleft);
            }
        });
    }
}
