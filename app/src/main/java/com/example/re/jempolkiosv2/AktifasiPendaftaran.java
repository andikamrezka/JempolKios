package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AktifasiPendaftaran extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aktifasi_pendaftaran);
        TextView tv_ap = (TextView) findViewById(R.id.tv_ap);
        tv_ap.setText(" Hallo, Mitra \n \n " +
                "Sebuah Email/Pesan berisi Kode verifikasi telah \n " +
                "dikirim ke email yang Anda daftarkan. Bila Anda \n " +
                "melalukan pendaftaran menggunakan Nomor \n " +
                "Handphone, silahkan periksa pesan masuk \n \n " +
                "Silahkan Masukan Kode Aktifasi yang Anda terima" +
                "\n");

        Button sukses = (Button) findViewById(R.id.sukses);

        sukses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AktifasiPendaftaran.this, RegistrasiBerhasil.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromright, R.anim.slidetoleft);
            }
        });
    }
}
