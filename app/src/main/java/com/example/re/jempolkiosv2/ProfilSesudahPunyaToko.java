package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class ProfilSesudahPunyaToko extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_sesudah_punya_toko);

        LinearLayout terkutuk = (LinearLayout) findViewById(R.id.terkutuk);

        terkutuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intent = new Intent(ProfilSesudahPunyaToko.this, DashboardToko.class);
                startActivity(Intent);
                overridePendingTransition(R.anim.slidefromleft, R.anim.slidetoright);
            }
        });
    }
}
