package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

public class DashboardToko extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_toko);

        RelativeLayout btndatabrg = (RelativeLayout) findViewById(R.id.btndatabrg);
        RelativeLayout btncashier = (RelativeLayout) findViewById(R.id.btncashier);

        btncashier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardToko.this, Cashier.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromleft, R.anim.slidetoright);
            }
        });

        btndatabrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardToko.this, MasterBarang.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromleft, R.anim.slidetoright);
            }
        });
    }
}
