package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FormBuatAkun extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_buat_akun);

        Button buat = (Button) findViewById(R.id.buat);

        buat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FormBuatAkun.this, AktifasiPendaftaran.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromright, R.anim.slidetoleft);
            }
        });
    }
}
