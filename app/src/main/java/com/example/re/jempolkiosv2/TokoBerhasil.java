package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TokoBerhasil extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toko_berhasil);
        TextView tv_tb = (TextView) findViewById(R.id.tv_tb);
        tv_tb.setText(" Hallo, Kios \n \n " +
                "Selamat Toko Anda berhasil didaftarkan. Kini Anda\n " +
                "dapat mengelola toko fisik. Anda dan Toko Online\n " +
                "Anda langsung dari genggaman tangan.\n \n " +
                "Sebagai permulaan Anda dapat mulai.\n " +
                "\n");

        LinearLayout atur = (LinearLayout) findViewById(R.id.atur);
        LinearLayout tambahbrg = (LinearLayout) findViewById(R.id.tambahbrg);

        tambahbrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TokoBerhasil.this, MasterBarang.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromright, R.anim.slidetoleft);
            }
        });

        atur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TokoBerhasil.this, DashboardToko.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromright, R.anim.slidetoleft);
            }
        });
    }
}
