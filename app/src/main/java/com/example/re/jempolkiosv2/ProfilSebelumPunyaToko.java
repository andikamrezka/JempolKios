package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ProfilSebelumPunyaToko extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_sebelum_punya_toko);
        Button buat = (Button) findViewById(R.id.buat);

        buat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfilSebelumPunyaToko.this, FormPendaftaranToko.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromright, R.anim.slidetoleft);
            }
        });
    }
}
