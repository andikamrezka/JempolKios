package com.example.re.jempolkiosv2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buat_akun = (Button) findViewById(R.id.buat_akun);
        Button masuk = (Button) findViewById(R.id.masuk);

        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FormMasuk.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromtop, R.anim.slidetobot);
            }
        });

        buat_akun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FormBuatAkun.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slidefromtop, R.anim.slidetobot);
            }
        });
    }
}
